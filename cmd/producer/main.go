package main

import (
	
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"log"
	"fmt"
)

func main(){
	deliveryChan := make(chan kafka.Event)
	producer := NewKafkaProducer()
	Publish("transferencia" , "teste", producer, []byte("transferencia"), deliveryChan)
	go DelivieryReport(deliveryChan)
	// e := <-deliveryChan
	// msg := e.(*kafka.Message)

	
	producer.Flush(15000)
}

func NewKafkaProducer() *kafka.Producer{
	configMap := &kafka.ConfigMap{
		"bootstrap.servers": "gokafka-kafka-1:9092",
		"delivery.timeout.ms": "0",
		"acks": "all",
		"enable.idempotence": "true",
	}

	p, err := kafka.NewProducer(configMap)
	if err != nil{
		log.Println(err.Error())
	}

	return p
}

func Publish(msg string, topic string, producer *kafka.Producer, key []byte, deliveryChan chan kafka.Event) error{
	message := &kafka.Message{
		Value: []byte(msg),
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Key: key,
	}

	err := producer.Produce(message, deliveryChan)
	if err != nil{
		return err
	}

	return nil
}

func DelivieryReport(deliveryChan chan kafka.Event){
	for e := range deliveryChan{
		switch ev := e.(type){
		case *kafka.Message:
			if ev.TopicPartition.Error != nil {
				fmt.Println("Erro ao enviar")
			}else{
				fmt.Println("Mensagem enviada", ev.TopicPartition)
			}
		}
	}
}